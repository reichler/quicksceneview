Hooks.once("ready", async () => {
    Hooks.on('getSceneDirectoryEntryContext', (html, entryOptions) => {
        viewOption = {
            name: "View",
            icon: '<i class="far fa-eye"></i>',
            condition: li => game.user.isGM && !game.scenes.get(li.data("entityId")).data.active,
            callback: li => {
              const scene = game.scenes.get(li.data("entityId"));
              scene.view();
            }
        };
        entryOptions.unshift(viewOption);
    });
});