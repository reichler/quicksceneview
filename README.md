# QuickSceneView

Foundry VTT module that adds a Scene Directory context menu entry to directly "View" a scene without activating it, or having to first add it to the scene navigation list.

![scene context menu with View button](img/sceneContextMenu.png)

# Manifest

[https://gitlab.com/reichler/quicksceneview/-/raw/master/quicksceneview/module.json](https://gitlab.com/reichler/quicksceneview/-/raw/master/quicksceneview/module.json)
